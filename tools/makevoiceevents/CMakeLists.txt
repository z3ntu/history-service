set(makevoiceevents_SRCS main.cpp)

include_directories(
    ${CMAKE_SOURCE_DIR}/src
    )

add_executable(lomiri-history-makevoiceevents ${makevoiceevents_SRCS})
qt5_use_modules(lomiri-history-makevoiceevents Core)

target_link_libraries(lomiri-history-makevoiceevents lomirihistoryservice)
install(TARGETS lomiri-history-makevoiceevents RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
